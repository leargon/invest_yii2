<?php

namespace app\controllers;


use yii\web\Controller;
use app\models\Projects;
use yii\data\Pagination;


class ProjectsController extends Controller {
	public function actionIndex(){
		$query = Projects::find();
		$pagination = new Pagination([
			'defaultPageSize' => 5,
			'totalCount' => $query->count(),
		]);

		$projects = $query->orderBy('id')
			->offset($pagination->offset)
			->limit($pagination->limit)
			->all();
		return $this->render('index', [
			'projects' => $projects,
			'pagination' => $pagination,
		]);
	}
}